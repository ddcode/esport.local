import Vue from "vue";
import Router, {
  Route
} from "vue-router";

// Global
import NotFound from "./components/NotFound.vue";

// Players
import PlayersDisplayAll from "./components/players/DisplayAll.vue";
import PlayersDisplayDetailed from "./components/players/DisplayDetailed.vue";
import PlayersCreate from "./components/players/Create.vue";
import PlayersEdit from "./components/players/Edit.vue";

// Teams
import TeamsDisplayAll from "./components/teams/DisplayAll.vue";
import TeamsDisplayDetailed from "./components/teams/DisplayDetailed.vue";
import TeamsCreate from "./components/teams/Create.vue";
import TeamsEdit from "./components/teams/Edit.vue";

// Matches
import MatchesDisplayAll from "./components/matches/DisplayAll.vue";
import MatchesDisplayDetailed from "./components/matches/DisplayDetailed.vue";
import MatchesCreate from "./components/matches/Create.vue";
import MatchesEdit from "./components/matches/Edit.vue";

// Tournaments
import TournamentsDisplayAll from "./components/tournaments/DisplayAll.vue";
import TournamentsDisplaySingle from "./components/tournaments/DisplaySingle.vue";
import TournamentsCreate from "./components/tournaments/Create.vue";
import TournamentsEdit from "./components/tournaments/Edit.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,

  routes: [{
      path: "/",
      redirect: {
        name: "players-all",
      },
    },
    {
      path: "/players",
      name: "players-all",
      component: PlayersDisplayAll,
    },
    {
      path: "/players/create",
      name: "player-create",
      component: PlayersCreate,
    },
    {
      path: "/players/:slug",
      name: "player-details",
      component: PlayersDisplayDetailed,
    },
    {
      path: "/players/:slug/edit",
      name: "player-edit",
      component: PlayersEdit,
    },
    {
      path: "/teams",
      name: "teams-all",
      component: TeamsDisplayAll,
    },
    {
      path: "/teams/create",
      name: "team-create",
      component: TeamsCreate,
    },
    {
      path: "/teams/:slug",
      name: "team-details",
      component: TeamsDisplayDetailed,
    },
    {
      path: "/teams/:slug/edit",
      name: "team-edit",
      component: TeamsEdit,
    },
    {
      path: "/matches",
      name: "matches-all",
      component: MatchesDisplayAll,
    },
    {
      path: "/matches/create",
      name: "match-create",
      component: MatchesCreate,
    },
    {
      path: "/matches/:slug",
      name: "match-details",
      component: MatchesDisplayDetailed,
    },
    {
      path: "/matches/:slug/edit",
      name: "match-edit",
      component: MatchesEdit,
    },
    {
      path: "/tournaments",
      name: "tournaments-display",
      component: TournamentsDisplayAll,
    },
    {
      path: "/tournaments/create",
      name: "tournaments-create",
      component: TournamentsCreate,
    },
    {
      path: "/tournaments/:slug",
      name: "tournaments-display-single",
      component: TournamentsDisplaySingle,
    },
    {
      path: "/tournaments/:slug/edit",
      name: "tournaments-edit",
      component: TournamentsEdit,
    },
    {
      path: "*",
      redirect: {
        name: "404",
      },
    },
    {
      path: "/404",
      name: "404",
      component: NotFound,
    },
  ],
});

export default router;